import matplotlib.pyplot as plt
import numpy


def rysuj(n):
    pi = numpy.pi
    xV = []
    yV = []
    for i in range(n+1):
        angle = 2*pi*i/n
        x = numpy.cos(angle)
        y = numpy.sin(angle)

        xV.append(x)
        yV.append(y)

    plt.xlim(-2, 2)
    plt.ylim(-2, 2)
    plt.grid()

    plt.plot(xV, yV, 'b-o', lw=2)
    plt.axhline(y=0, color='black', lw=1)
    plt.axvline(x=0, color='black', lw=1)
    plt.fill_between(xV, yV, color="lightblue")

    plt.show()


def main():
    print("Podaj liczbe pierwiastkow:")
    n = int(input())
    rysuj(n)


main()
