import fileinput


def addMultRows(matrix, rowToChange, rowOrigin, mult):
    for i in range(len(matrix[0])):
        matrix[rowToChange][i] += matrix[rowOrigin][i] * mult
        if abs(matrix[rowToChange][i]) < 0.000001:
            matrix[rowToChange][i] = 0
    return matrix


def swapRows(matrix, row1, row2):
    matrix[row1], matrix[row2] = matrix[row2], matrix[row1]
    return matrix


def divideByConstant(matrix, row, div):
    for i in range(len(matrix[0])):
        matrix[row][i] /= div
    return matrix


def firstValInCol(matrix, col, row):
    while row < len(matrix):
        if matrix[row][col] != 0:
            return row
        row += 1
    return "empty"


def reduceUpperRows(matrix, row, col):
    for reducedRow in range(row):
        matrix = addMultRows(matrix, reducedRow, row, -matrix[reducedRow][col])
    return matrix


def rowEmpty(matrix, row):
    for col in range(len(matrix[0]) - 1):
        if matrix[row][col] != 0:
            return False
    return True


def postacSchodkowa(matrix):
    leftColumn = 0
    for row in range(len(matrix)):
        # message(matrix, "Iteration:")

        # if leftColumn is filled with zeroes
        # counting from current row down,
        # move leftColumn to the right
        while (
            leftColumn < len(matrix[0]) - 1
            and firstValInCol(matrix, leftColumn, row) == "empty"
        ):
            leftColumn += 1
            if leftColumn == len(matrix[0]) - 1:
                # if every column is empty you can't make
                # the mattrix more reduced therefore return it
                return matrix
        # pushing row with value up

        matrix = swapRows(matrix, row, firstValInCol(matrix, leftColumn, row))
        # changing the first value to one in the row
        matrix = divideByConstant(matrix, row, matrix[row][leftColumn])
        # changing every other value to 0 in that column
        for zeroedRow in range(row + 1, len(matrix)):
            if matrix[zeroedRow][leftColumn] != 0:
                matrix = addMultRows(
                    matrix, zeroedRow, row, -matrix[zeroedRow][leftColumn]
                )

        if leftColumn < len(matrix[0]) - 2:
            leftColumn += 1
    return matrix


def postacSchodkowaNormalna(matrix):
    matrix = postacSchodkowa(matrix)
    # matrix is in an echelon form
    # so we just remove value above 1's
    # message(matrix, "Before reducing")

    for row in range(len(matrix) - 1, -1, -1):
        for col in range(len(matrix[0]) - 1):
            if matrix[row][col] == 1:
                matrix = reduceUpperRows(matrix, row, col)
                break

    return matrix


def message(matrix, text):
    print("\n", text)
    for row in matrix:
        print(row[:-1], [row[-1]])


def getInput():
    # for removing duplicate spaces
    import re

    return [
        [int(c.replace("\n", "")) for c in re.sub(" +", " ", line).split(" ")]
        for line in fileinput.input(files="macierzeInput.txt")
    ]


def infiniteAnswers(matrix):
    # sprawdzam czy jest parametr w jakiejś linii
    for row in matrix:
        leading_one = False
        for col in range(len(row) - 1):
            if leading_one and row[col] != 0:
                return True
            if row[col] == 1:
                leading_one = True


def checkLinearProperties(matrix):
    check = 0
    for row in range(len(matrix) - 1, -1, -1):
        if rowEmpty(matrix, row) and matrix[row][-1] != 0:
            check = "nie ma, jest sprzeczny"
            break
    if check == 0:
        if infiniteAnswers(matrix):
            check = "nieskonczenie wiele"
        # for row in range(len(matrix)):
        #     if colWithOneAndOther(matrix, row):
        #         check = "nieskonczenie wiele"
        #         break
    if check == 0:
        check = "ma jedno rozwiazanie:"
        col = 0
        for i in range(len(matrix)):
            while col < len(matrix[0]):
                if matrix[i][col] == 1:
                    check += "\n {x" + str(col + 1) + "=" + str(matrix[i][-1])
                    break
                col += 1

    print("\n[ROZWIAZANIA]:", check)


def prettify(matrix):
    for i in range(len(matrix)):
        for j in range(len(matrix[0])):
            if matrix[i][j] == -0.0:
                matrix[i][j] = 0
            else:
                matrix[i][j] = round(matrix[i][j], 5)
    return matrix


def generateTestCases(n, size_range=[3, 10], val_range=[-10, 10]):
    import random

    matrices = []
    for _ in range(n):
        size = [
            random.randint(size_range[0] + 1, size_range[1] + 1),
            random.randint(size_range[0], size_range[1]),
        ]

        temp_matrix = [[0] * size[0] for _ in range(size[1])]

        for i in range(len(temp_matrix)):
            for j in range(len(temp_matrix[0])):
                temp_matrix[i][j] = random.randint(val_range[0], val_range[1])

        matrices.append(temp_matrix)

    return matrices


def main():
    # format of input: last column is row value
    matrices = [getInput()]
    # matrices = generateTestCases(10, [4, 6])
    for matrix in matrices:

        message(matrix, "Input:")
        matrix = postacSchodkowaNormalna(matrix)
        message(prettify(matrix), "Output:")

        checkLinearProperties(matrix)

        print("-=" * 35 + "-")
    # nieskonczenie wiele rozwiazan jesli istnieje jakas wartosc
    # oprocz jedynki w rzedzie

    # sprzeczny jesli w rzedzie sa same zera i jednoczesnie ma
    # stala inna niz 0

    # ma rozwiazania jesli nie spelnia dwoch poprzednich warunkow
    # szukam wiodacych jedynek w kazdym rzedzie i printuje np.
    # x2,x3,x5 zaleznie tez od tego czy sa same zera w kolumnach
    # dlatego czasem omijam np 1,4
    # 0 1 0 0 0 [1]
    # 0 0 1 0 0 [2]
    # 0 0 0 0 1 [3]


main()
