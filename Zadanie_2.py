import matplotlib.pyplot as plt
import numpy


def rysuj(n):
    x=[0,n.real]
    y=[0,n.imag]

    plt.title("Wektor liczby zespolonej")
    plt.arrow(x=0,y=0,dx=n.real,dy=n.imag,color='red',head_width=0.13)
    plt.xlim(-abs(n.real)-1,abs(n.real)+1)
    plt.ylim(-abs(n.imag)-1,abs(n.imag)+1)
    # plt.xlabel('Moduł='+str(round((n.real**2+n.imag**2)**(1/2),3))+"    Arg="+str(round(numpy.arctan(n.imag/n.real),3))+"(rad)")
    plt.axhline(y=0,color='black')
    plt.axvline(x=0,color='black')
    
    plt.legend(['Moduł='+str(round((n.real**2+n.imag**2)**(1/2),3)),"Arg="+str(round(numpy.arctan(n.imag/n.real)*180/numpy.pi,3))+"(stopni)"])
    plt.grid()
    plt.show()

def main():
    print("Podaj liczbę zespoloną:")
    n=complex(input())
    rysuj(n)
main()